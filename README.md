# Watchfire

A personal fork of the Soothing 32 texture pack created for Minetest, following
the similar styling and palette constraints as the original pack created by
Zughy32 and the contributors listed in this README. This pack focuses on 
compatibility with MineClone2.

The texture pack is 32 colours only, more exactly the ones in the
[Zughy32 palette](https://lospec.com/palette-list/zughy-32).

![Soothing 32](screenshot.png)  

### Official supported mods
* Beds
* Bones
* Bucket
* Carts
* Default
* Doors
* Flowers
* NextGen Bows
* Wool
* (chess only) X-Decor-libre

### Submitted by contributors (that maybe someday I'll tweak)
* (partly) 3D Armor (Giov4)
* Butterflies (tinnéh)
* Dye (tinnéh)
* Fire (tinnéh)
* Fireflies (tinnéh)
* (partly) Mesecons (Giov4)
* Screwdriver (tinnéh)
* Stairs (tinnéh)
* Tnt (tinnéh)
* Vessels (tinnéh)
* Xpanes (tinnéh)

